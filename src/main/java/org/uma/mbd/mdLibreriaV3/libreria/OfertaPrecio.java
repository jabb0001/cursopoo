package org.uma.mbd.mdLibreriaV3.libreria;

public class OfertaPrecio implements OfertaFlex{

    private double porcentaje, umbral;

    public void OfertaPrecio(double porc, double umb){
        porcentaje = porc;
        umbral = umb;
    }

    @Override
    public double getDescuento(Libro libro) {
        if(libro.getPrecioBase() >= umbral){
            return porcentaje;
        } else {
            return 0;
        }
    }

    @Override
    public String toString(){
        return porcentaje + "% (" + umbral + ")";
    }
}
