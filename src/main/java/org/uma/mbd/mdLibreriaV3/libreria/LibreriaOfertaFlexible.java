package org.uma.mbd.mdLibreriaV3.libreria;

public class LibreriaOfertaFlexible extends Libreria {
    private double precioBase;
    private OfertaFlex ofertaFlexible;





    public LibreriaOfertaFlexible(OfertaFlex oferta){
        this(TAM_DEFECTO, oferta);
    }

    public LibreriaOfertaFlexible ( int tam, OfertaFlex oferta){
        super(tam);
        ofertaFlexible = oferta;
    }

    @Override
    public void addLibro(String aut, String tit, double precBase){
        Libro libro = new Libro(aut, tit, precBase);
        double desc = ofertaFlexible.getDescuento(libro);
        if (desc > 0){
            libro = new LibroEnOferta(aut, tit, precBase, desc);
        }
        addLibro(libro);

    }

    @Override
    public String toString(){
        return ofertaFlexible + super.toString();
    }


}
