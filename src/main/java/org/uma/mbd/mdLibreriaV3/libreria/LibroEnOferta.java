package org.uma.mbd.mdLibreriaV3.libreria;

import org.uma.mbd.mdLibreriaV3.libreria.Libro;

public class LibroEnOferta extends Libro {

    private double descuento;
    private double px = getPrecioBase() - ((getPrecioBase() * descuento) / 100);
    public LibroEnOferta(String au, String ti, double pb, double desc){
        super(au, ti, pb);
        desc = descuento;
    }

    public double getDescuento(){
        return descuento;
    }


    @Override
    public double getPrecioFinal(){
        return px + px * IVA / 100;
    }

    @Override
    public String toString(){
        return "(" + getAutor()
                + "; " + getTitulo()
                + "; " + getPrecioBase()
                + "; " + descuento + "%"
                + "; " + px
                + "; " + IVA + "%;"
                + getPrecioFinal() + ")";
    }
}
