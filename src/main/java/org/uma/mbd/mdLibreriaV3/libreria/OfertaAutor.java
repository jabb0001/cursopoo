package org.uma.mbd.mdLibreriaV3.libreria;

import java.util.Arrays;

public class OfertaAutor implements OfertaFlex {
    private String[] autor;
    private double porcentaje;


    public OfertaAutor(double porc, String[] aut) {
        autor = aut;
        porcentaje = porc;
    }

    @Override
    public double getDescuento(Libro libro) {
        int i = 0;
        while (i < autor.length && !(libro.getAutor().equalsIgnoreCase(autor[i]))) {
            i++;
        }
        return (i == autor.length) ? 0 : porcentaje;
    }

    @Override
    public String toString() {

        return porcentaje + "% " + Arrays.toString(autor);

    }
}

