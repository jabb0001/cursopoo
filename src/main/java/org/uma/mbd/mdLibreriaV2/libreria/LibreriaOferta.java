package org.uma.mbd.mdLibreriaV2.libreria;

import org.uma.mbd.mdLibreriaV1.libreria.Libreria;
import org.uma.mbd.mdLibreriaV1.libreria.Libro;

import java.util.Arrays;

public class LibreriaOferta extends Libreria {

    private double descuento;
    private String [] oferta;

    public LibreriaOferta(double des, String [] aut){
        this(TAM_DEFECTO, des, aut);
    }

    public LibreriaOferta(int tam, double des, String [] aut){
        super(tam);
        descuento = des;
        oferta = aut;
    }

    public void setOferta(double desc, String [] aut){
        descuento = desc;
        oferta = aut;
    }

    private boolean esAutorEnOferta(String autor){
        int i = 0;
        while (i < oferta.length && ! autor.equalsIgnoreCase(oferta[i])) {
        i++;
        }
        return i < oferta.length;
    }

    @Override
    public void addLibro(String autor, String titulo, double pb){
        Libro libro = null;
      if (esAutorEnOferta(autor)){
          libro = new LibroEnOferta(autor, titulo, pb, descuento);
      } else {
          libro = new Libro(autor, titulo, pb);
      } addLibro(libro);
    }

    @Override
    public String toString(){
        return descuento + "% " + Arrays.toString(oferta) + super.toString();
    }

}
