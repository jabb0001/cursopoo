package org.uma.mbd.mdTest.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class TestAsignatura {
    private String asignatura;
    private Test[] examen;
    private double valorAciertos, valorErrores;

    public TestAsignatura(String asig, double valAc, double valErr, String[] valTot){
        asignatura = asig;
        valorAciertos = valAc;
        valorErrores = valErr;
        String[] cadena = valTot;
        examen = new Test[cadena.length];
        int pos = 0;
        for (String e : cadena){
            String[] txtseparado = e.split("[:+]+");
            for(String i: txtseparado) {
                String nombreAlumno = txtseparado[0];
                Integer entero1 = new Integer(txtseparado[1]);
                Integer entero2 = new Integer(txtseparado[2]);
                examen[pos] = new Test(nombreAlumno, entero1, -entero2);
            }
            pos++;
        }

    }

    public TestAsignatura(String asig, String[] valTot){
        asignatura = asig;
        valorAciertos = 1;
        valorErrores = 0;
        String[] cadena = valTot;
        examen = new Test[cadena.length];
        int pos = 0;
        for (String e : cadena){
            String[] txtseparado = e.split("[:+]+");
            String nombreAlumno = txtseparado[0];
            Integer entero1 = new Integer(txtseparado[1]); //Preguntar lo que ha dicho del Integer(parseint)?
            Integer entero2 = new Integer(txtseparado[2]);
            examen[pos] = new Test(nombreAlumno, entero1, -entero2);
            pos++;

        }
    }

    public TestAsignatura(String asig, double va, double vf){
        asignatura = asig;
        valorAciertos = va;
        valorErrores = vf;
    }

    public void leeDatos(String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner(new File(file))){
            leeDatos(sc);
        }
    }

    public void leeDatos(Scanner sc){
        examen = new Test[5];
        int pos = 0;
        while (sc.hasNext()){
            String linea = sc.nextLine();
            Test test = stringToTest(linea);
            if(pos==examen.length){
                examen = Arrays.copyOf(examen,examen.length);
            }
            examen[pos] = test;
            pos++;
        }
        examen = Arrays.copyOf(examen, pos);
    }

    private Test stringToTest(String linea){
        try (Scanner sc = new Scanner(linea)){
            sc.useDelimiter("[;+]");
            String nombreAl = sc.next();
            int ac = sc.nextInt();
            int er = sc.nextInt();
            Test test = new Test(nombreAl,ac,er);
            return test;
        }
    }

    public void extraeDatos(String[] datos){
        examen = new Test[datos.length];
        for (int i = 0; i < datos.length; i++){

            }
        }
    }

    public String getNombre(){
        return asignatura;
    }

    public double notaMedia(){
        double restotal = 0;
        for (int i = 0; i < examen.length; i++){
            restotal += ((valorAciertos * examen[i].aciertos) - (valorErrores * examen[i].errores));
        }
        return restotal / examen.length;
    }

    public int aprobados(){
        double resultado;
        int numAprob = 0;
        for (int i = 0; i < examen.length; i++){
            resultado = ((valorAciertos * examen[i].aciertos) - (valorErrores * examen[i].errores));
            if (resultado >= 5){
                numAprob++;
            }
        }
        return numAprob;
    }

    public Test [] testaprobados(){
        Test [] testap = new Test[examen.length];
        int pos = 0;
        for (Test test: examen) {
            if (test.calificacion(valorAciertos,valorErrores) >= 5){
                testap[pos] = test;
                pos++;
            }
        }
        return Arrays.copyOf(testap, pos);
    }


    /*public Test alumnosAprobados(){

        for (Test e: examen){
            if (e.calificacion(valorAciertos,valorErrores)){

            }
        }
    }*/


}
