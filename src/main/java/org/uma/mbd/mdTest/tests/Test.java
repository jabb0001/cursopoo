package org.uma.mbd.mdTest.tests;

public class Test {
    protected String alumno;
    protected int aciertos, errores;

    public Test(String nom, int ac, int err){
        alumno = nom;
        aciertos = ac;
        errores = err;
    }

    public String getNombre() {
        return alumno;
    }

    public int getAciertos() {
        return aciertos;
    }

    public int getErrores() {
        return errores;
    }

    @Override
    public boolean equals(Object obj){
        boolean res = obj instanceof Test;
        Test t = res ? (Test) obj : null;
        return res && alumno.equalsIgnoreCase(t.alumno);
    }

    @Override
    public int hashCode(){
        return alumno.toLowerCase().hashCode();
    }

    @Override
    public String toString(){
        return alumno.toUpperCase() + "[" + aciertos + ", " + errores + "]";
    }

    public double calificacion(double valAc, double valErr){
        if (valAc <= 0 || valErr > 0){
            throw new IllegalArgumentException("La valoración de los aciertos debe ser estrictamente mayor que cero, y además la valoración de los errores debe ser menor o igual que cero.");
        }
        double nota = aciertos * valAc + errores * valErr;
        return nota;
    }
}
