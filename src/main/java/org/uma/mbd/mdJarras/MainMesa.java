package org.uma.mbd.mdJarras;
import org.uma.mbd.mdJarras.mesa.Mesa;
import org.uma.mbd.mdJarras.jarras.Jarra;

public class MainMesa {
    public static void main(String[] args){
        Mesa mesa1 = new Mesa(7,5);
        mesa1.llenaA();
        mesa1.vuelcaAsobreB();

        System.out.println(mesa1.mesaToString());
    }
}
