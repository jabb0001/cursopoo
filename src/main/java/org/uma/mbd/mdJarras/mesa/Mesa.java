package org.uma.mbd.mdJarras.mesa;

import org.uma.mbd.mdJarras.jarras.Jarra;

public class Mesa {

        private int capacidad1, capacidad2;
        private Jarra jarra1 = new Jarra(capacidad1);
        private Jarra jarra2 = new Jarra(capacidad2);

        public Mesa(int capacidad1, int capacidad2){
            jarra1.setCapacidad(capacidad1);
            jarra2.setCapacidad(capacidad2);
            jarra1.setContenido(0);
            jarra2.setContenido(0);

        }
        public void llenaA(){
            jarra1.llena();

        }

        public void llenaB(){
            jarra2.llena();
        }

        public void vaciaA(){
            jarra1.vacia();
        }
        public void vaciaB(){
            jarra2.vacia();
        }

        public void vuelcaAsobreB(){
            int cabe2 = jarra2.getCapacidad() - jarra2.getContenido();
            if (cabe2 >= jarra1.getContenido()){
                jarra2.setContenido(jarra2.getContenido() + jarra1.getContenido());
                jarra1.setContenido(0);
            } else {
                jarra2.setContenido(jarra2.getCapacidad());
                jarra1.setContenido(jarra1.getContenido() - cabe2);
            }
        }

        public void vuelcaBsobreA(){
            int cabe1 = jarra1.getCapacidad() - jarra1.getContenido();
            if (cabe1 >= jarra2.getContenido()){
                jarra1.setContenido(jarra1.getContenido() + jarra2.getContenido());
                jarra2.setContenido(0);
            } else {
                jarra1.setContenido(jarra1.getCapacidad());
                jarra2.setContenido(jarra2.getContenido() - cabe1);
            }
        }

        public int getContenido1(){
            return jarra1.getContenido();
        }
        public int getContenido2(){
            return jarra2.getContenido();
        }
        public int getCapacidad1(){
            return jarra1.getCapacidad();
        }
        public int getCapacidad2(){
            return jarra2.getCapacidad();
        }
        public int getContenidoMesa(){
            return jarra1.getContenido() + jarra2.getContenido();
        }
        public String mesaToString(){
        return "J(" + jarra1.getCapacidad() + ", " + jarra1.getContenido() + ")"
                + "J(" + jarra2.getCapacidad() + ", " + jarra2.getContenido() + ")";
    }
    }

