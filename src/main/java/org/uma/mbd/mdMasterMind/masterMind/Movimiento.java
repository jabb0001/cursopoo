package org.uma.mbd.mdMasterMind.masterMind;

public class Movimiento {
    private int colocadas, descolocadas;
    private String cifras;
    public Movimiento(String cif, int col, int desc){
        cifras = cif;
        colocadas = col;
        descolocadas = desc;
    }

    public int getColocadas(){
        return colocadas;
    }

    public int getDescolocadas(){
        return descolocadas;
    }

    public String getCifras() {
        return cifras;
    }

    @Override
    public boolean equals(Object obj){
        boolean res = obj instanceof Movimiento;
        Movimiento m = res ? (Movimiento)obj : null;
        return res && m.cifras.equalsIgnoreCase(cifras);
    }

    @Override
    public int hashCode(){
        return cifras.hashCode();
    }

    @Override
    public String toString(){
        return "[" + cifras + ", " + colocadas + ", " + descolocadas + "]";
    }

}
