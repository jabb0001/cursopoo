package org.uma.mbd.mdJarrasMezcla;

import org.uma.mbd.mdJarras.jarras.Jarra;
import org.uma.mbd.mdJarrasMezcla.jarrasMezcla.JarrasMezcla;

public class MainJarrasMezcla {
    public static void main(String[] args){

        JarrasMezcla ja = new JarrasMezcla(7);
        JarrasMezcla jb = new JarrasMezcla(8);
        Jarra jarraAgua = new Jarra(5);

        jarraAgua.setContenido(3);
        jb.llenaDesde(jarraAgua);
        jb.llenaVino();
        ja.llenaDesde(jb);
        // Esto es un comentario





        System.out.println("La jarra A es: " + ja.toString());
        System.out.println("La jarra B es: " + jb.toString());





    }
}
