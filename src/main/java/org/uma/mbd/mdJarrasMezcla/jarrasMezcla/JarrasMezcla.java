package org.uma.mbd.mdJarrasMezcla.jarrasMezcla;
import org.uma.mbd.mdJarras.jarras.Jarra;
public class JarrasMezcla extends Jarra{

    private double pureza;
    private int contenido;


    public JarrasMezcla(int cap){
        super(cap);
        pureza = 0;
        contenido = 0;
    }

    @Override
    public void llena(){
        int cabe = this.getCapacidad() - contenido;
        contenido = this.getCapacidad();
        pureza = (contenido * pureza)/ this.getCapacidad();
    }

    public void llenaVino(){
        int cabe = getCapacidad() - contenido;
        pureza = ((contenido * pureza) + (100 * cabe)) / getCapacidad();
        contenido = getCapacidad();
    }

    @Override
    public void llenaDesde(Jarra j){
        int cabe = getCapacidad() - contenido;
        if (cabe >= j.getContenido()){
            pureza = (contenido * pureza) / (contenido + cabe);
            contenido += j.getContenido();
            j.setContenido(0);
        } else {
            pureza = (contenido * pureza) / getCapacidad();
            contenido = getCapacidad();
            j.setContenido(j.getContenido() - cabe);
        }
    }


    public void llenaDesde(JarrasMezcla j){
        int cabe = getCapacidad() - contenido;
        if (cabe >= j.contenido){
            pureza = ((contenido * pureza) + (j.pureza * cabe)) / (contenido + cabe);
            contenido += j.contenido;
            j.contenido = 0;
            j.pureza = 0;
        } else {
            pureza = ((contenido * pureza) + (j.pureza * cabe)) / getCapacidad();
            contenido = getCapacidad();
            j.contenido -= cabe;
        }
    }
    @Override
    public String toString(){
        return "J(" + this.getCapacidad() + ", " + contenido + ", " + pureza + ")";
    }

}
